\documentclass[aspectratio=169]{beamer}
\usepackage{preamble}

\subtitle{week 1: Introduction to Software Development in Teams}
\author{slides: Kilian Evang}
\date{}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\begin{frame}
    \tableofcontents[hideallsubsections]
\end{frame}

\section[Teams]{Software Development in Teams}

\frame{\tableofcontents[currentsection,hideallsubsections]}


\begin{frame}{Software Development in Teams}
    \begin{itemize}
        \item Communication is key!
        \item Teams need to agree on:
            \begin{enumerate}
                \item functional requirements
                \item non-functional requirements
                    %\item interfaces and contracts
                        %\item documentation
                        %\item unit tests
                \item division of labor
                \item coding best practices
                \item how to exchange code
            \end{enumerate}
    \end{itemize}
\end{frame}

\begin{frame}{Things to Agree on (1): Functional Requirements}
    \begin{itemize}
        \item \structure{Functional requirements}:
            \begin{itemize}
                \item What should the program do?
                \item What are its capabilities?
            \end{itemize}
        \item Example:
            \begin{itemize}
                \item user enters a number \lstinline{b} and
                    an exponent \lstinline{n}, \\
                    the program outputs the result of $b^n$
                \item afterwards, it asks the user whether to
                    calculate another power
            \end{itemize}
        \item Functional requirements typically given by customer
    \end{itemize}
\end{frame}

\begin{frame}{Things to Agree on (2): Non-functional requirements}
 \begin{itemize}
 \item \structure{Non-functional requirements}:
     \begin{itemize}
        \item Internal structure of the program
        \item Architecture
     \end{itemize}
 \item Example:
 \begin{itemize}
      \item program uses a function called \lstinline+power()+
          which takes two arguments:
          (1) the base \lstinline{b} and
          (2) the exponent \lstinline{n}; computes $b^n$ and returns the result
      \item the program should consist of distinct components
          ``model, view, controller''
 \end{itemize}
  \item Non-functional requirements often given by lead developer
  \item UML diagrams can be used to make the architecture clear before
      starting to code
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Interfaces as Contracts}
 \begin{itemize}
  \item Person who \structure{writes} a component should clearly communicate to
        the person who \structure{uses} a component how to use it and what to
        expect.
  \item i.e.: What classes and methods are available,
        with what arguments they should be called,
         and what they do (interface).
    \item Such information should go into \structure{docstrings}; e.g.:
\begin{lstlisting}[basicstyle=\ttfamily\footnotesize,language=Python]
def power(base, exponent):
    """Raise `base` to the power of `exponent`."""
    ...
\end{lstlisting}
  \item It's like a \structure{contract}:
  \begin{itemize}
   \item ``If you promise to call this method with valid arguments,
       I promise that you will get the correct answer.''
  \end{itemize}
 \end{itemize}
\end{frame}


\begin{frame}{Things to Agree on (3): Division of Labor}
 \begin{itemize}
 \item Programs should be \structure{modular}.
  \item Modularity enables clear division of labor: \\
        each team member takes responsibility for one or more components.
  \item E.g., a mortgage calculator, team of three with one responsibility each:
    \begin{enumerate}
        \item User interface
        \item Database
        \item Calculations
    \end{enumerate}
 \end{itemize}
\end{frame}


\section{Coding best practices}
\frame{\tableofcontents[currentsection,hideallsubsections]}

\begin{frame}{Things to Agree on (4): Coding best practices}
    \begin{itemize}
        \item Minimize Complexity
        \item Documentation
        \item Unit Tests
        \item Coding style
        \item Code review
    \end{itemize}
\end{frame}

\begin{frame}{Minimize complexity}
    \begin{itemize}
        \item Simple is better than complex
        \item \structure{LOC}: The less lines of code the better
        \item \structure{DRY}: Don't Repeat Yourself
        \item \structure{YAGNI}: You Ain't Gonna Need It
    \end{itemize}
\end{frame}


\subsection{Documentation}
\begin{frame}{Types of documentation}
    \begin{description}
        \item[User guide] how to install, start, operate
        \item[API] documents each function/class
        \item[Comments] explains non-obvious code
    \end{description}
\end{frame}

\begin{frame}[fragile]{A correct and properly documented version}
 Minimal, a one-line description:
 \begin{lstlisting}[language=python]
def power(base, exponent):
    """Raise `base` to the power of `exponent`."""
    return base ** exponent
 \end{lstlisting}
 Learn more about docstrings: \\
 \url{http://www.pythonforbeginners.com/basics/python-docstrings}
\end{frame}


\begin{frame}[fragile]{Document parameters}
 \begin{lstlisting}[language=python]
def power(base, exponent):
    """Exponentiate two numbers.
    
    :param base: a number
    :param exponent: a number
    :returns: `base` raised to the power of `exponent`
    """
    return base ** exponent
 \end{lstlisting}

    See \url{https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html}
\end{frame}


\begin{frame}[fragile]{Include an example (doctest)}
 \begin{lstlisting}[language=python]
def power(base, exponent):
    """Exponentiate two numbers.
    
    :param base: a number
    :param exponent: a number
    :returns: `base` raised to the power of `exponent`
    
    >>> power(2, 3)
    8
    """
    return base ** exponent
 \end{lstlisting}

 A doctest is useful both as documentation and test:
 
 \begin{lstlisting}
 python3 -m doctest power.py
 \end{lstlisting}
\end{frame}

\subsection{Unit tests}
\begin{frame}{Unit Tests}
 \begin{itemize}
  \item Does the program adhere to the functional requirements? \\
      Does it fulfill the contract?
  \item Define \structure{test cases}, check automatically if code behaves correctly
  \item \structure{Unit}: a single function or class
  \item Cannot find \structure{all} bugs, but many
  \item Useful for checking you didn't break the code after making changes
  \item Will be used for grading your homework
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Defining a Unit Test}
 File \lstinline+test_power.py+:
\begin{lstlisting}[language=python]
import power

def test_power(self):
    assert power.power(2, 7) == 128
    assert power.power(3, 2) == 9
\end{lstlisting}

\begin{itemize}
    \item A function whose name begins with \lstinline{test_} defines a unit test
    \item The unit test contains one or more \lstinline{assert} statements that are checked
\end{itemize}

See \url{https://docs.pytest.org/}
\end{frame}

\begin{frame}[fragile]{Running Unit Tests}
\begin{lstlisting}
$ ls
power.py    test_power.py
$ pytest
...
2 passed in 0.1 seconds
\end{lstlisting}
 \begin{itemize}
  \item Runs all tests in files \lstinline+test_*.py+ in current directory
  \item Tells you which tests succeed and which fail
  \item Shows the expected and actual output
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Cheating on Unit Tests?}
 \begin{lstlisting}[language=python]
def power(base, exponent):
    if base == 2 and exponent == 7:
        return 128
    if base == 3 and exponent == 2:
        return 9
 \end{lstlisting}
 \begin{itemize}
  \item Program passes test but does not fulfill contract
  \item Easily caught by using additional test cases
  \item Don't do this
 \end{itemize}
\end{frame}

% describe other tests:
% integration tests
% regression tests
% show real world examples, such as from roaringbitmap


\subsection{Coding style}
\begin{frame}{Style Conventions}
 \begin{itemize}
  \item The coding style should be consistent within one project.
  \item E.g.: should attribute and method names use under\_scores or camelCase?
  \item E.g.: how many spaces to use for indentation? (4.)
  \item For this course, use this style guide (Python de-facto standard):
        \url{https://www.python.org/dev/peps/pep-0008/}
  \item \texttt{pycodestyle} command-line tool can partially check style
      automatically
 \end{itemize}
\end{frame}

\begin{frame}{Why does style matter?}
    Messy code is \dots
    \begin{itemize}
        \item Harder to read
        \item Harder to spot mistakes
        \item Harder to maintain / improve
    \end{itemize}
\end{frame}

\begin{frame}{Code review}
    \begin{itemize}
        \item Code review: one or more co-workers provide feedback \\
            before a change is accepted
        \item Empirically, this is the most effective method to avoid bugs
    \end{itemize}

    \vspace{1em}
    Extreme case: \url{https://www.fastcompany.com/28121/they-write-right-stuff}
\end{frame}


\section[Git]{Version Control with Git}

\frame{\tableofcontents[currentsection,hideallsubsections]}

\begin{frame}{Things to Agree on (5): Exchanging Code}
 How do team members get the code each other has written?
 \begin{itemize}
  \item Send them around via email? Messy, error-prone.
  \item Dropbox or similar? No good either:
  \begin{itemize}
   \item Alice saves a file in the middle of something,
       suddenly Bob can't run the program anymore.
   \item Alice and Bob edit a file at the same time, chaos ensues.
   \item Very hard to find out who did what when, and why.
  \end{itemize}
  \item No excuse for not using a \structure{version control system} (VCS)!
 \end{itemize}
\end{frame}


\begin{frame}{What Are Version Control Systems?}
 Version control systems (VCSs) are essential tools for collaborative
 development of software (and documentation, etc.). They
 \begin{itemize}
  \item allow multiple people to work on the same files/directories without
        stepping on each other's toes
  \item keep a full history of all changes (similar to Wikipedia page history)
  \item make it possible to look up who changed what when, and why
  \item make it possible to revert bad changes
  \item ...
 \end{itemize}
\end{frame}

\begin{frame}{Repositories and Revisions}
 \begin{center}
  \includegraphics[height=4cm]{fig/ch02dia7}
 \end{center}
 \begin{itemize}
  \item a \structure{repository} is a directory shared between a team whose
      content changes over time
  \item the state of the repository at a given time is called a \structure{revision}
 \end{itemize}

 \tiny{Diagram from The SVN Book}
\end{frame}

\begin{frame}{Centralized Setup}
 \begin{center}
  \includegraphics[width=5cm]{fig/centralized}\\
 \end{center}
 \begin{itemize}
  \item One copy of the repository is kept on a server 
  \item Every team member has her/his own local copy of the repository
  \item Copies are kept in sync by periodically
  \begin{itemize}
   \item pushing your own changes to the server
   \item pulling others' changes from the server
  \end{itemize}
 \end{itemize}
 \tiny{Diagram from The SVN Book}
\end{frame}

%\begin{frame}
% \frametitle{File Servers vs. Centralized VCSs}
% \begin{itemize}
%  \item A centralized VCS allows all team members to upload and download files, like an ordinary file server.
%  \item In addition, it keeps a history.
%  \item In addition, it provides a mechanism for properly handling \emph{concurrent modification} -- i.e. two team members modifying the same file at the same time.
% \end{itemize}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Problem to Avoid}
% \begin{center}
%  \includegraphics[height=6cm]{fig/ch02dia2}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Copy-Modify-Merge Solution}
% \begin{center}
%  \includegraphics[height=6cm]{fig/ch02dia4}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}
%
%\begin{frame}
% \frametitle{Concurrent Modification: The Copy-Modify-Merge Solution (cont.)}
% \begin{center}
%  \includegraphics[height=6cm]{fig/ch02dia5}
% \end{center}
%
% \tiny{Diagram from The SVN Book}
%\end{frame}

\begin{frame}[fragile]{Git}
 \begin{itemize}
  \item Git: the most widely used VCS
  \item Free, open source
  \item Most important commands: \texttt{status, add, commit, pull, push}
  \item Docs: \url{https://git-scm.com/}  \hspace{2em} Tutorial: \url{https://try.github.io/}
  \item Will be used for all your homework assignments, group project
 \end{itemize}
\end{frame}

\begin{frame}{A Simple Git Workflow}
 \begin{enumerate}
  \item \structure{Initialize} a repository or \structure{clone} an existing one.
  \item Make changes
  \item \structure{Stage} changes for commit
  \item \structure{Commit} the added changes with a message
  \item \structure{Pull} others' changes from the origin, (resolve conflicts, commit
        the resolution)
  \item \structure{Push} your commits to a server so others can pull them
  \item Repeat from 2
 \end{enumerate}
\end{frame}

\subsection{Git Workflow in Detail}

\begin{frame}[fragile]{Step 1: Clone a Repository from a Server}
 \begin{lstlisting}[basicstyle=\ttfamily]
$ git clone https://alice@bitbucket.org/alice/buffet.git
Cloning into 'buffet'...
remote: Counting objects: 3, done.
remote: Total 3 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (3/3), done.
Checking connectivity... done.
 \end{lstlisting}
\end{frame}

%\begin{frame}
% \frametitle{Aside: Repositories}
% \begin{itemize}
%  \item A Git \emph{repository} contains the files belonging to a project,
%        including the history with earlier versions
%  \item Every team member has their own copy of the repository, or even more
%        (e.g. one on university computer, one on laptop)
%  \item One central copy on a server is usually used to exchange codes between
%        team members
% \end{itemize}
%\end{frame}

\begin{frame}{Step 2: Make Changes}
 \begin{itemize}
  \item Create and modify files as you normally would
  \item E.g.: Add more information to \lstinline{README.md}, add \lstinline{script.py}
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{\texttt{git status}}
\vspace{-1ex}
\begin{lstlisting}[basicstyle=\ttfamily\footnotesize]
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

    modified:   README.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

    script.py

no changes added to commit (use "git add" and/or "git commit -a")
 \end{lstlisting}
 \vspace{-1ex}
 \begin{itemize}
  \item \texttt{git status} is your friend. \\
      Use this command frequently to see the status of your repository.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Step 3: Stage Changes}
 \begin{lstlisting}
$ git add README.md
$ git add script.py
 \end{lstlisting}
 \begin{itemize}
  \item To stage new/modified files, use \lstinline+git add+
  \item To remove/rename tracked files, use \lstinline+git rm+/\lstinline+git mv+
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{\texttt{git status}}
 \begin{lstlisting}
$ git status
On branch master
Your branch is up-to-date with 'origin/master'.

Changes to be committed:
  (use "git reset HEAD <file>..." to unstage)

    modified:   README.md
    new file:   script.py

 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{See what you staged}
 \begin{lstlisting}
$ git diff --staged
diff --git a/README.md b/README.md
index ec91dae..9c84284 100644
--- a/README.md
+++ b/README.md
@@ -1 +1,3 @@
 buffet
 ======
+
+A repository full of delicious foods
 \end{lstlisting}
 ...
 \begin{itemize}
  \item \lstinline{git diff} shows more detail than \lstinline{git status}:
      changes inside files
  \item Note: to see changes you did \structure{not} yet stage,
      leave out \lstinline{--staged}.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Step 4: Commit Changes}
 \begin{lstlisting}[basicstyle=\ttfamily]
$ git commit -m "Add description to README, add script"
[master 9c84284] Add description to README, add script
 2 files changed, 6 insertions(+)
 create mode 100644 script.py
 \end{lstlisting}
\end{frame}

\begin{frame}{Commits}
 \begin{itemize}
  \item Each commit should represent one logical step in the development
        of your application, e.g. fix \structure{one} bug
        or add \structure{one} new feature
  \item If you commit too rarely, others work with outdated code, \\
          risk of merge conflicts increases
  \item If you commit before you are finished with one task, others may work
        with intermediate, non-working code
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Commit messages}
Commit messages are important!
\begin{itemize}
\item Explains others what you did and \structure{why}
\item Useful for future reference
\end{itemize}

\begin{lstlisting}[basicstyle=\ttfamily\footnotesize]
$ git commit -m "Simple, one-line commit message"
$ git commit
# [You will go to an editor and can type a longer message]
\end{lstlisting}

Guidelines:
\begin{itemize}
\item First line (header) provides a meaningful summary
\item Separate header from body with a blank line
%\item Limit the subject line to 50 characters
%\item Capitalize the subject line
%\item Do not end the subject line with a period
%\item Use the imperative mood in the header line
%\item Wrap the body at 72 characters
\item Use the body to explain what and why (not how)
\end{itemize}

See \url{https://chris.beams.io/posts/git-commit/}
\end{frame}





\begin{frame}[fragile]{\texttt{git status}}
 \begin{lstlisting}
$ git status
On branch master
Your branch is ahead of 'origin/master' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working directory clean
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Step 5: Pull from Remote Repository}
 \begin{lstlisting}
$ git pull
remote: Counting objects: 5, done.
remote: Compressing objects: 100% (5/5), done.
remote: Total 5 (delta 0), reused 0 (delta 0)
Unpacking objects: 100% (5/5), done.
From https://bitbucket.org/kevang/buffet
   b5f8cf6..498a504  master     -> origin/master
Merge made by the 'recursive' strategy.
 logo.jpg | Bin 0 -> 13509 bytes
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 logo.jpg
 \end{lstlisting}
 \begin{itemize}
  \item Another team member added a logo while we were working!
  \item Git automatically merges the changes.
  \item If Git cannot merge automatically, it will ask you to do so manually
 \end{itemize}
\end{frame}

\begin{frame}[fragile] %{\texttt{git log}}
\begin{columns}[T]
\column{0.6\linewidth}
\vspace{-1ex}
 \begin{lstlisting}[basicstyle=\scriptsize\ttfamily]
$ git log
commit bd7c030a2fc49ed10a123a90cc70a483be0b7c15
Merge: 9c84284 498a504
Author: Alice <alice@example.com>
Date:   Fri Jan 15 15:01:36 2016 +0100

    Merge branch 'master' of https://bitbucket.org/alice/buffet

commit 9c8428412061775ca7dcb1f58c49e8f590d691b2
Author: Alice <alice@example.com>
Date:   Fri Jan 15 15:00:57 2016 +0100

    Added description to README, added script

commit f5698ed8b52089aee040d968c008c2e6858d0e9e
Author: Bob <bob@example.com>
Date:   Fri Jan 15 14:52:18 2016 +0100

    Added logo

commit ec91daed05da85834bbf0ec180d58bed941892b6
Author: Alice <alice@example.com>
Date:   Fri Jan 15 11:59:23 2016 +0100

    Initial commit with contributors
 \end{lstlisting}
\column{0.4\linewidth}
 \begin{itemize}
     \item \lstinline{git log} shows you the history (from new to old): \\
      who did what when?
  %\item Use \lstinline{git log --stat} for details about file changes
  \item Use \lstinline{git log -p} to see all differences between files
 \end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Step 6: Push Your New Commits to Server}
 \begin{lstlisting}[basicstyle=\ttfamily\footnotesize]
$ git push
Password for 'https://alice@bitbucket.org': 
Counting objects: 9, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 812 bytes | 0 bytes/s, done.
Total 6 (delta 0), reused 0 (delta 0)
To https://alice@bitbucket.org/alice/buffet.git
   498a504..bd7c030  master -> master
 \end{lstlisting}
 \begin{itemize}
     \item Now other team members can get your changes (pull)
 \end{itemize}
\end{frame}

\section{Resolving Conflicts}

\frame{\tableofcontents[currentsection,hideallsubsections]}

\begin{frame}{Resolving Conflicts}
 \begin{itemize}
  \item{Concurrent modification in Git}
  \begin{itemize}
   \item you work on a file in your working directory 
   \item in the meantime, a coworker pushes a new version of this file to
         the server
   \item before you can push your version, you have to pull
   \item pull creates a merged version
  \end{itemize}
 \end{itemize}
\end{frame}

\begin{frame}{Resolving Conflicts (cont.)}
 \begin{itemize}
  \item{Creating a merged version}
  \begin{itemize}
   \item the challenge: combine your changes and your colleague's changes in
         the repository together into one file
     \item if changes are in different parts of the file,
         Git \structure{merges} automatically
   \item otherwise, manual intervention may be required: a \structure{conflict}
  \end{itemize}
 \end{itemize}
\end{frame}

\subsection{An Example}

\begin{frame}[fragile]{Conflicts: an Example}
 Suppose Jim and Sally are using Git to make a Sandwich together. \\
 They both check out this initial version of \lstinline{sandwich.txt}:
 \begin{lstlisting}
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Conflicts: an Example (cont.)}
 Jim adds some toppings in his working copy:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{red}{Salami}+
+\textcolor{red}{Mortadella}+
+\textcolor{red}{Prosciutto}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Conflicts: an Example (cont.)}
 Meanwhile, Sally adds some other toppings in her working copy:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{OliveGreen}{Sauerkraut}+
+\textcolor{OliveGreen}{Grilled chicken}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}{Conflicts: an Example (cont.)}
 Then this happens:
 \includegraphics[height=6cm]{fig/cartoon}\\
 \tiny{Cartoon: \url{http://geek-and-poke.com}, CC-BY}
\end{frame}

\begin{frame}[fragile]{Conflicts: an Example (cont.)}
 \begin{itemize}
  \item Sally was the first to push her version.
  \item Jim will have to resolve the conflict.
  \item Jim \structure{cannot push} his changes.
  \item When he pulls, he sees this:
 \end{itemize}
 \begin{verbatim}
$ git pull
Auto-merging sandwich.txt
CONFLICT (content): Merge conflict in sandwich.txt
Automatic merge failed; fix conflicts and then commit the
result.
 \end{verbatim}
\end{frame}

\begin{frame}[fragile]{Resolving a Conflict}
 The file with the conflict now contains \structure{conflict markers}. In its
 present state, it is not usable:

 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
<<<<<<< HEAD
+\textcolor{red}{Salami}+
+\textcolor{red}{Mortadella}+
+\textcolor{red}{Prosciutto}+
=======
+\textcolor{OliveGreen}{Sauerkraut}+
+\textcolor{OliveGreen}{Grilled chicken}+
>>>>>>> f14d784ad0f0dd9cf2cc5bdf5db92136d4468512
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}{Resolving a Conflict (cont.)}
\begin{itemize}
    \item Remember: communication is key!
    \item Team members need to agree on how a conflict should be resolved.
        \begin{itemize}
            \item Sometimes it's obvious.
            \item Sometimes you need to discuss.
        \end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Resolving a Conflict (cont.)}
 \begin{itemize}
  \item Jim needs to talk to Sally about what to put on the sandwich.
  \item All off their toppings? Might be a bit much.
  \item Also, you won't get sauerkraut at their favorite Italian deli.
  \item How about a compromise: prosciutto and grilled chicken,
      discard the other additions.
 \end{itemize}
\end{frame}

\begin{frame}[fragile]{Resolving a Conflict (cont.)}
 After reaching an agreement with Sally, Jim edits the file
    and---importantly---removes the conflict markers:
 \begin{lstlisting}[escapechar=+]
Top piece of bread
Mayonnaise
Lettuce
Tomato
Provolone
+\textcolor{OliveGreen}{Prosciutto}+
+\textcolor{OliveGreen}{Grilled Chicken}+
Creole Mustard
Bottom piece of bread
 \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Resolving a Conflict (cont.)}
 Finally, he tells Git that the conflict is resolved by adding the
 manually merged file. Then, commit and push as usual.
 \begin{lstlisting}[basicstyle=\ttfamily]
$ git add sandwich.txt
$ git commit -m "Compromise sandwich agreed on with Sally."
$ git push
...
 \end{lstlisting}
\end{frame}


\section{This course}

\frame{\tableofcontents[currentsection]}

\begin{frame}{Course overview}
    To pass the course:
    \begin{itemize}
        \item 4 Individual assignments (30\%)
        \item Group project (40\%)
        \item Stay-at-home exam (30\%)
    \end{itemize}
    Need to have at least 5.5 for each part!

    Only exam has a resit.
\end{frame}


% \begin{frame}{All materials are on git too}
% \begin{itemize}
%     \item Course syllabus (studiehandleiding)
%     \item Slides
%     \item Assignments
%     \item Project
% \end{itemize}
% 
% Note:
% 
% \begin{itemize}
% \item Adapted from material by Kilian Evang, Gertjan van Noord
% \end{itemize}
% 
% 
% \end{frame}
% 
% \begin{frame}[fragile]{Getting Started}
% 
% {
% \begin{lstlisting}[basicstyle=\ttfamily]
% git clone https://bitbucket.org/andreasvc/gevpro.git
% \end{lstlisting}
% }
% 
% \begin{itemize}
% \item You get a local copy of all course materials
% \item Often do a \verb+git pull+ to ensure you have
%     the latest additions and changes
% \item the first assignment is in assignments/week1/
% \end{itemize}
% 
% 
% \end{frame}


\begin{frame}{Assignments: tests and style}
Avoid deduction of points!
\begin{itemize}
    \item Run the tests with \lstinline{pytest}

        \structure{All} tests must pass.

    \item Check style with \lstinline{pycodestyle}

        There should be \structure{no} errors or warnings.
\end{itemize}
\end{frame}



\end{document}
