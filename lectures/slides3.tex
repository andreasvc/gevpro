\documentclass[aspectratio=169]{beamer}
\usepackage{preamble}

\subtitle{week 3: Debugging, Profiling, Data Science}
\author{Andreas van Cranenburgh}
\date{}

\begin{document}
\maketitle

\begin{frame}{Why not graphical user interfaces?}
Course description promises graphical interfaces; but \dots
\begin{description}
\item[Command line] interface: easy, this course
\item[Web] interface: doable, other course
\item[Graphical] interface: hard!
    \begin{itemize}
        \item Requires lots of code for simple things
        \item Each platform works differently: \\
            Windows, Mac, Linux, Android, iPhone
    \end{itemize}
\end{description}

\pause
Therefore, let's cover something else which is
definitely useful for your studies and career!
\end{frame}


\begin{frame}
\tableofcontents
\end{frame}



\section{Improving code}
\subsection{Working code}
\frame{\tableofcontents[currentsection]}
\begin{frame}{Working code}
    \begin{definition}
        \structure{Debugging}: identify and fix errors (`bugs') in programs
    \end{definition}

    \pause\vspace{1em}
    \begin{columns}
        \column{0.5\linewidth}
            ``If debugging is the art of removing bugs from programs,
            programming must be the art of inserting them''
            (attributed to Edsger Dijkstra)

            % If you want more effective programmers, you will discover that they should not waste their time debugging, they should not introduce the bugs to start with.
            % (Edsger Dijkstra)
        \column{0.5\linewidth}
            \includegraphics[height=0.5\textheight]{fig/dijkstra}

            \vspace{1em}
            Interview: \url{https://youtu.be/mLEOZO1GwVc}
    \end{columns}
\end{frame}


% \begin{frame}
%     % On two occasions I have been asked, --- "Pray, Mr. Babbage, if you put
%     % into the machine wrong figures, will the right answers come out?" In one
%     % case a member of the Upper, and in the other a member of the Lower House
%     % put this question. I am not able rightly to apprehend the kind of
%     % confusion of ideas that could provoke such a question.
% 
%     % Charles Babbage, Passages from the Life of a Philosopher (1864), ch. 5:
%     % "Difference Engine No. 1"
% 
%     \begin{columns}
%         \column{0.7\linewidth}
%     Maurice Wilkes discovers debugging in 1949:
% 
%     \vspace{1em}
%     \begin{quote}\normalfont
%     ``As soon as we started programming, we found to our surprise that it
%     wasn't as easy to get programs right as we had thought. Debugging had to be
%     discovered. I can remember the exact instant when I realized that a large
%     part of my life from then on was going to be spent in finding mistakes in
%     my own programs.''
%     \end{quote}
%         \column{0.3\linewidth}
%     \end{columns}
%     % \pause\vspace{1em}
%     % ``Everyone knows that debugging is twice as hard as writing a program in
%     % the first place. So if you're as clever as you can be when you write it,
%     % how will you ever debug it?'' (Brian Kernighan)
% \end{frame}


\begin{frame}{Common errors}
    \begin{description}[AttributeError]
        \item[SyntaxError] unclosed parenthesis, missing comma, colon etc.
        \item[NameError] typo in variable/function
        \item[IndexError] \texttt{seq[n] with n >= len(seq)}
        \item[KeyError] using key not in dict
        \item[AttributeError] \texttt{obj.att}, but obj does not have att
        \item[TypeError] mixing str/int/etc.
        \item[ValueError] generic error: right type, wrong value
        \item[\dots] \dots
    \end{description}
\end{frame}

\begin{frame}[fragile]{Understanding tracebacks}
\begin{definition}
    \structure{Traceback}: the chain of function calls that led to an error.
\end{definition}
\begin{lstlisting}
>>> from collections import Counter
>>> c = Counter('abcdef')
>>> c.most_common('a')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/usr/lib/python3.7/collections/__init__.py", line 584, in most_common
    return _heapq.nlargest(n, self.items(), key=_itemgetter(1))
  File "/usr/lib/python3.7/heapq.py", line 546, in nlargest
    if n >= size:
TypeError: '>=' not supported between instances of 'str' and 'int'
\end{lstlisting}
\pause
First step: locate the \structure{root cause}.
Typically somewhere in your own code \dots
\end{frame}

\begin{frame}{General strategy}
    The most effective debugging tool is still careful thought, coupled with
    judiciously placed print statements. (Brian Kernighan)
    
    \pause\vspace{1em}
    Go through each line of code \dots
    \begin{itemize}
        \item Think like the computer: \\
            what is happening? \\
            Is it what you intended?
        \item Ask yourself: what is the input and output? \\
            Confirm: print values
        \item Does the problem only occur with particular values? \\
            What is special about them?
        \item For difficult problems, repeat several times.
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Using a debugger}
A debugger allows you to step through the code and look at variables

\vspace{1em}
Start script with debugger:
\begin{lstlisting}
$ pdb3 yourscript.py  # enter "c" to start the script
\end{lstlisting}

Or add the following at a specific spot in your code:
\begin{lstlisting}
breakpoint()
\end{lstlisting}

    Commands:
    \begin{description}
        \item[p expr] print the value of an expression
        \item[ll] show source code for current function
        \item[u/d] go up or down in the stack trace
        \item[n] run next line of current function
        \item[s] run next line, stop inside other function
    \end{description}

More documentation: \url{https://docs.python.org/3/library/pdb.html}
\end{frame}

\begin{frame}[fragile]{pdb example}
\begin{columns}[T]
\column{0.5\linewidth}
\begin{lstlisting}[basicstyle=\footnotesize\ttfamily, numbers=left]
"""Extract adjectives from Cornetto lexicon."""
import sys
import xml.etree.ElementTree as ET

def get_adjectives(filename):
    """Get set of adjectives in [...]"""
    tree = ET.parse(filename)
    root = tree.getroot()
    return {cid.attrib['Form'] for cid in root
            if cid.attrib['pos'] == 'ADJ'}

def main():
    print(get_adjectives(sys.argv[1]))

if __name__ == '__main__':
    main()
\end{lstlisting}
\pause
\column{0.5\linewidth}
\begin{lstlisting}[basicstyle=\scriptsize\ttfamily,language=, keywordstyle=\color{black}, identifierstyle=\color{black}, stringstyle=\color{black},]
$ pdb3 cdb.py cdb-sample.xml
> cdb.py(1)<module>()
-> """Extract adjectives from Cornetto lexicon."""
(Pdb) c
Traceback (most recent call last):
  [...]
  File "cdb.py", line 13, in main
    print(get_adjectives(sys.argv[1]))
  File "cdb.py", line 9, in get_adjectives
    return {cid.attrib['Form'] for cid in root
  File "cdb.py", line 10, in <setcomp>
    if cid.attrib['pos'] == 'ADJ'}
KeyError: 'Form'
Uncaught exception. Entering post mortem debugging
Running 'cont' or 'step' will restart the program
> cdb.py(10)<setcomp>()
-> if cid.attrib['pos'] == 'ADJ'}
\end{lstlisting}
\pause\vspace{-1em}
\begin{lstlisting}[basicstyle=\scriptsize\ttfamily,language=, keywordstyle=\color{black}, identifierstyle=\color{black}, stringstyle=\color{black},]
(Pdb) cid.attrib
{'cid_id': '165802', 'd_sy_id': 'n_a-523296', 'form': 'nietig', 'c_lu_id': 'd_a-184600', 'pos': 'ADJ', 'seq_nr': '3', 'status': 'manual', 'selected': 'true', 'name': 'attila'}
(Pdb) cid.attrib['form']
'nietig'
(Pdb)
\end{lstlisting}
\end{columns}
\end{frame}


\subsection{Good code}
\begin{frame}{Good code: Refactoring}
    Insight: can achieve the same end in many different ways

    \pause
    \begin{definition}
        \structure{Refactoring}: improving code (e.g., readability) \\
            while keeping the same functionality (i.e., behavior).
    \end{definition}

    % FIXME: already mentioned in previous lectures
    % concrete examples, or leave out.
    \begin{itemize}
        \item Clear variable names
        \item Separate steps/functions
        \item Use idioms
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Example: working code}
\begin{lstlisting}
def clean_tweets(filename):
    with open(filename, encoding='utf8') as infile:
        text = infile.read()
        cleaned_text = ''
        lower_text = text.lower()
        for character in lower_text:
            if character not in '!$%^&*()-+={}[]:;"\'|<>,.?/~`':
                cleaned_text += character
        cleaned_list = cleaned_text.split()
        return cleaned_list
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Example: re-factored}
\begin{columns}
\column{0.6\linewidth}
\begin{lstlisting}
def remove_punc(text):
    punctuation = '!$%^&*()-+={}[]:;"\'|<>,.?/~`'
    return ''.join(character for character in text
            if character not in punctuation)

def clean_tweets(filename):
    with open(filename, encoding='utf8') as infile:
        text = infile.read()
    cleaned_text = remove_punc(text.lower())
    tokens = cleaned_text.split()
    return tokens
\end{lstlisting}
\column{0.4\linewidth}
\begin{itemize}
\item Separate function for punctuation
\item More efficient punctuation elimination
\item Limit scope of \textcolor{blue}{\texttt{with}}
\item Renamed \textcolor{blue}{\texttt{cleaned\_list}} \\
    to \textcolor{blue}{\texttt{tokens}}
\end{itemize}
\end{columns}
\end{frame}

% \begin{frame}{Code style guides}
%     \begin{reference}
%         PEP-0008: coding conventions for Python code.
%         \url{https://www.python.org/dev/peps/pep-0008/}
%     \end{reference}
%     Follow a style guide, e.g.,
%     \begin{itemize}
%         \item Max. 1000 lines per file
%         \item Descriptive variable names
%         \item Avoid spaghetti \& ravioli code
%         \item etc.
%     \end{itemize}
%     % FIXME: not really applicable to notebook code
%     
%     \structure{But}:
%         ``A Foolish Consistency is the Hobgoblin of Little Minds''
%         (Ralph Waldo Emerson, Self-Reliance)
% \end{frame}



\subsection{Fast code}
\begin{frame}[fragile]{Fast code: Example}
    \begin{columns}[T]
\column{0.4\linewidth}
\begin{lstlisting}
def counter(words):
    counts = {}
    for word in words:
        if word not in counts:
            counts[word] = 0
        counts[word] += 1
    return counts
\end{lstlisting}
% OR:
% from collections import Counter
% return Counter(words)
\column{0.6\linewidth}
\begin{lstlisting}
def count_in_list(word_to_count, words):
    number_of_hits = 0
    for word in words:
        if word == word_to_count:
            number_of_hits += 1
    return number_of_hits

def counter2(words):
    counts = {}
    unique_words = set(words)
    for word in unique_words:
        counts[word] = count_in_list(word, words)
\end{lstlisting}
% OR: counts[word] = words.count(word)
\end{columns}
\end{frame}


\begin{frame}{Levels to optimize}
    \begin{description}
        \item[Hardware] get faster machine, more memory, etc.
        \item[Code] improve `constant' factors \\ (caching, compact representations etc.)
        \item[Algorithm] e.g., sorting by shuffling will never be fast \\ (algorithmic complexity)
        \item[Problem] e.g., sorting is harder than searching \\ (computational complexity)
    \end{description}

    \pause
    \begin{itemize}
        \item Avoid doing unnecessary things
        \item Avoid slow methods
        \item Lots of clever things \dots (rabbit hole)
    \end{itemize}
\end{frame}

\begin{frame}{Efficiency}
    \centering\includegraphics[height=0.6\textheight]{fig/xkcdefficiency}

    \vspace{1em}
    \url{https://www.xkcd.com/1445/}
\end{frame}


\begin{frame}{Profiling}
    \begin{definition}
        \structure{Profiling}: identify performance bottlenecks by measuring
    \end{definition}
    
    Two important measurements:
    \begin{itemize}
        \item How often is each function called
        \item How long does it take?
    \end{itemize}

    \pause\vspace{1em}
    Consider two scenarios:
    \begin{enumerate}
        \item A 100x speedup of a component responsible for 1\% of total runtime.
        \item A 1.1x speedup of a component responsible for 90\% of total runtime.
    \end{enumerate}
    Do not waste your time on 1; therefore: measure!
\end{frame}

% \begin{frame}{Is it worth the time?}
%     \hfill\includegraphics[height=0.9\textheight]{fig/xkcdworththetime}
% 
%         \scriptsize
%         \vspace{1em}
%         \url{https://www.xkcd.com/1205/}
% \end{frame}

\begin{frame}[fragile]{Profiling with Python: timeit}
            The \lstinline+timeit+ module runs a line of code multiple times, \\
            and reports how long it takes.

\begin{lstlisting}
>>> import timeit
>>> setup = 'text = "sample string"; char = "g"'
>>> timeit.timeit('char in text', setup=setup)
0.41440500499993504
>>> timeit.timeit('text.find(char)', setup=setup)
1.7246671520006203
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Profiling with Python: cProfile}
\lstinline+cProfile+ gives a detailed report on which functions
take the most time.

\begin{lstlisting}[style=plain]
$ python -m cProfile -s time wordsearch.py
   169650 function calls (169549 primitive calls) in 0.173 seconds

   Ordered by: internal time

   ncalls  tottime  percall  cumtime  percall filename:lineno(function)
    83026    0.117    0.000    0.117    0.000 wordsearch.py:13(hit)
        1    0.011    0.011    0.020    0.020 wordsearch.py:29(<listcomp>)
        1    0.011    0.011    0.011    0.011 decoder.py:345(raw_decode)
        1    0.011    0.011    0.128    0.128 wordsearch.py:37(<listcomp>)
[...]
\end{lstlisting}

        Focus on the top 10.

        \vspace{1em}
        More information: \url{https://julien.danjou.info/guide-to-python-profiling-cprofile-concrete-case-carbonara/}
\end{frame}

\begin{frame}{Takeaway}
    ``We should forget about small efficiencies, say about 97\% of the time:
    premature optimization is the root of all evil. 
    
    \pause
    Yet we should not pass up our opportunities in the critical 3\%''
    -- Donald Knuth

    \vspace{1em}
    \begin{itemize}
        \item Consider expected return on investment
        \item Optimization risks overcomplicating \& over-engineering
    \end{itemize}
\end{frame}

\begin{frame}{Summary}
    Improving code involves
    \begin{itemize}
        \item Debugging
        \item Re-factoring, code style
        \item Optimization, profiling
    \end{itemize}
\end{frame}





\section{Data Science}
\frame{\sectionpage}

\begin{frame}{Definition}
    \begin{definition}
    \structure{Data science} is the application of
    computational and statistical techniques to
    address or gain [managerial or scientific]
    insight into some problem in the real world.
    --- Zico Kolter, Machine Learning Prof, CMU
    \end{definition}
\end{frame}


\begin{frame}
    \centering
    \includegraphics[height=0.8\textheight]{fig/venn}

    Drew Conway

    CEO, Alluvium (analytics company)
\end{frame}


\begin{frame}{The Data Lifecycle}
    \begin{enumerate}
        \item Data collection
        \item Data processing
        \item \structure{Exploratory analysis \& visualization}
        \item Analysis, hypothesis testing, \& Machine Learning
        \item Insight \& Policy Decision
    \end{enumerate}

    Today: focus on step 3
\end{frame}

\begin{frame}{Data Collection, Processing}
In practice, this often takes the most time (up to 80\%!)

\begin{itemize}
    \item Acquiring data
        \begin{itemize}
            \item Scrape from web, PDFs
            \item Database dump
            \item etc.
        \end{itemize}
    \item Messy data: encoding issues, date formats, order of first/last names, etc.
    \item Missing data
    \item Converting data
        \begin{description}
            \item[Tidy format] one observation per row, \\
                    each variable is a column
        \end{description}
\end{itemize}
\end{frame}





\begin{frame}{Austerity policy}
    Time line:
    \begin{itemize}
        \item 2008: Economic recession/crisis
        \item Reinhart \& Rogoff (2010) analyze correlation between debt and economic growth \\
            \begin{block}{Claim:}
            debt should be less than 90\% of GDP, otherwise economic growth drops dramatically
            \end{block}
        \item Austerity policy in EU, Greece needs to reduce debt etc.
    \end{itemize}
\end{frame}

\begin{frame}{The dangers of Excel}
    Problem:
    \begin{itemize}
        \item The claim is wrong. They made a crucial mistake in their Excel sheet!
        \item Excel hides formulas, easy to overlook mistakes
        \item Can we do better with Python?
    \end{itemize}

    \vspace{1em}
    {\footnotesize
    \url{https://www.nytimes.com/2013/04/19/opinion/krugman-the-excel-depression.html}}
\end{frame}

\begin{frame}{Reproducibility}
    Research should be reproducible:

    \begin{itemize}
        \item Code and data should be made available (if possible)
        \item Version control for code and data
        \item Ensure code still works 20+ years later. \\
                Hard problem: bit rot.
        \item Check code/data in peer review \\
            However, this requires lots of time and expertise; realistic?
    \end{itemize}
\end{frame}

\begin{frame}{The Python data science stack}
    \begin{itemize}
        \item Jupyter notebook
        %\item Numpy
        \item Pandas
        \item Matplotlib, Seaborn
    \end{itemize}
\end{frame}

\begin{frame}{Jupyter notebook}
    \includegraphics[height=0.9\textheight]{fig/jupyterpreview}
\end{frame}

\begin{frame}{Jupyter Notebook}
\begin{block}{Jupyter Notebook}
    A notebook keeps narrative, code, and results together.

    Ideally, everything can be reproduced with the push of a button.
    \end{block}
    \begin{itemize}
        \item Web-based interface
        \item Mix code, results, and explanation % \\ (Knuth: Literate programming)
        \item Include graphics / plots
        \item Interactive, rapid prototyping
    \end{itemize}
    %\url{https://jupyter.org/}

    \pause
    Pros and cons:
    \begin{itemize}
        \item Why Jupyter is data scientists' computational notebook of choice, Nature (2018).
            \url{https://www.nature.com/articles/d41586-018-07196-1}
        \item Why I don't like notebooks, Joel Grus (2018).
            \url{https://www.youtube.com/watch?v=7jiPeIFXb6U}
    \end{itemize}
\end{frame}

\begin{frame}{Notebook pitfalls}
    \begin{columns}
    \column{0.5\linewidth}
    \begin{itemize}
    \item Hidden state and out-of-order execution:\\
        Ensure code works by running all cells from start to finish!
        (Kernel $\rightarrow$ Restart \& Run All)
    \end{itemize}

    \column{0.5\linewidth}
    \includegraphics[width=0.95\textwidth]{fig/nboutoforder}
\end{columns}
\pause
\begin{itemize}
    \item Jupyter ``encourages'' throw-away code:\\
            Put re-usable code in a normal Python module and import. \\
            Apply software engineering practices:
            \begin{itemize}
                \item Ensure modular, documented code
                \item Check correctness with tests
            \end{itemize}
    \pause
    \item Can put Notebooks in version control, but differences hard to read
\end{itemize}

Despite this, notebooks are great for exploration and sharing results
\end{frame}

\subsection{Introduction to Pandas}
\frame{\subsectionpage}

\begin{frame}{Pandas}
    Pandas: high-performance, easy-to-use data structures \\
        and data analysis tools \dots

    \begin{itemize}
        \item Load/write tabular data (csv, excel)
        \item Manipulate/process data
        \item Analyze, plot, etc.
    \end{itemize}
\end{frame}

\begin{frame}{The main data structures}
    \begin{description}
        \item[Series:] one-dimensional, indexed array of numbers or strings
        \item[DataFrame:] two-dimensional table with Series as columns.
            \begin{itemize}
                \item Rows are instances (observations, individuals, etc.)
                \item Columns are features (e.g., age, length, etc.)
            \end{itemize}
                Each column is a series with a single type (homogeneous), \\
                but a data frame can contain columns of different types (heterogeneous).
    \end{description}
\end{frame}

\begin{frame}{The Telecom dataset}
    Rest of this section:
    \begin{itemize}
    \item Telecom dataset: why do we lose customers?
    \item Step-by-step walk-through of exploratory analysis.
    \end{itemize}

    \vspace{1em}
    Material from \url{https://mlcourse.ai/}
\end{frame}

\begin{frame}[fragile]{Reading/writing files}
Reading from a csv file.

\begin{lstlisting}
>>> import pandas as pd
>>> df = pd.read_csv('telecom_churn.csv')
>>> df.head()
\end{lstlisting}

\includegraphics[width=0.9\linewidth]{fig/telecom}

% Writing to a csv file.
%
% \begin{lstlisting}
% >>> df.to_csv('foo.csv')
\end{frame}

\begin{frame}[fragile]{Inspecting the shape and columns}
\begin{lstlisting}
>>> df.shape
(3333, 20)
\end{lstlisting}

3333 rows, 20 columns

\begin{lstlisting}
>>> df.columns
Index(['State', 'Account length', 'Area code', 'International plan', ...])
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{More information}
\begin{columns}
\column{0.65\textwidth}
\begin{lstlisting}
>>> df.info()
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 3333 entries, 0 to 3332
Data columns (total 20 columns):
State                     3333 non-null object
Account length            3333 non-null int64
Area code                 3333 non-null int64
International plan        3333 non-null object
Voice mail plan           3333 non-null object
Number vmail messages     3333 non-null int64
...
Total intl charge         3333 non-null float64
Customer service calls    3333 non-null int64
Churn                     3333 non-null bool
dtypes: bool(1), float64(8), int64(8), object(3)
memory usage: 498.1+ KB
\end{lstlisting}
\column{0.35\textwidth}
    \begin{itemize}
        \item Data types:
        \begin{itemize}
            \item boolean,
            \item integer,
            \item floating point,
            \item and object (strings)
        \end{itemize}
        \item No missing values (good)
    \end{itemize}
\end{columns}
\end{frame}

\begin{frame}[fragile]{Summary statistics}
    \begin{lstlisting}
    >>> df.describe()
    \end{lstlisting}

    \includegraphics[height=0.5\textheight]{fig/telecomdescribe}
\end{frame}

\begin{frame}[fragile]{Selecting a column}
\begin{lstlisting}
>>> df['Churn']
0       False
1       False
2       False
3       False
4       False
5       False
6       False
...
3330    False
3331    False
3332    False
Name: Churn, Length: 3333, dtype: bool
\end{lstlisting}

This column describes whether a client left or not.
\end{frame}


\begin{frame}[fragile]{Distribution of values}
    \begin{lstlisting}
    >>> df['Churn'].value_counts()
    False    2850
    True      483
    Name: Churn, dtype: bool
    \end{lstlisting}

    2850 clients out of 3333 are loyal

    \vspace{1em}\pause
    Calculate proportion:
    \begin{lstlisting}
    >>> df['Churn'].value_counts(normalize=True)
    False    0.855086
    True     0.144914
    Name: Churn, dtype: float64
    \end{lstlisting}

    14\% churn, very bad!

    \vspace{1em}
    Why are we losing customers?

\end{frame}

\begin{frame}[fragile]{Sorting by a column}
\begin{lstlisting}
>>> df.sort_values(by='Total day charge', ascending=False).head()
\end{lstlisting}

    \includegraphics[height=0.3\textheight]{fig/telecomsort}

    ascending=False: from high to low
\end{frame}

\begin{frame}[fragile]{Selecting slices}
    \begin{itemize}
        \item Indexing rows/columns by label:
\begin{lstlisting}
>>> df.loc[0:5, 'State':'Area code']
\end{lstlisting}

            NB: last element is included!

        \item Indexing rows/columns by integer location:

\begin{lstlisting}
>>> df.iloc[0:5, 0:3]
\end{lstlisting}

            \includegraphics[height=0.3\textheight]{fig/telecomslice}
    \end{itemize}
\end{frame}

\begin{frame}[fragile]{Boolean operations}
Which rows are clients on the international plan?

\begin{lstlisting}
>>> df['International plan'] == 'Yes'
0       False
1       False
2       False
3        True
4        True
5        True
6       False
7        True
8       False
...
3331     True
3332    False
Name: International plan, Length: 3333, dtype: bool
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Selecting particular rows}
\begin{lstlisting}
>>> df.loc[df['International plan'] == 'Yes', :]
   State  Account length  Area code International plan \
3     OH              84        408                Yes
4     OK              75        415                Yes
5     AL             118        510                Yes
7     MO             147        415                Yes
9     WV             141        415                Yes
38    AK             136        415                Yes
...
323 rows x 20 columns
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{What are the averages for those clients?}
\vspace{-0.5em}
\begin{lstlisting}
>>> df.loc[df['International plan'] == 'Yes', :].mean()
Account length            104.071207
Area code                 443.461300
Number vmail messages       8.464396
Total day minutes         187.986997
Total day calls           100.665635
Total day charge           31.958390
Total eve minutes         203.936842
Total eve calls           100.486068
Total eve charge           17.334923
Total night minutes       196.410217
Total night calls         100.851393
Total night charge          8.838483
Total intl minutes         10.628173
Total intl calls            4.609907
Total intl charge           2.869907
Customer service calls      1.464396
Churn                       0.424149
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Answering specific questions}
How much time (on average) do churned clients spend on the phone during daytime?

\begin{lstlisting}
>>> df.loc[df['Churn'] == 1, 'Total day minutes'].mean()
206.91407867494814
\end{lstlisting}

\pause
What is the maximum length of international calls among loyal clients
(Churn == 0) who do not have an international plan?

\begin{lstlisting}
>>> df.loc[
        (df['Churn'] == 0) & (df['International plan'] == 'No'),
        'Total intl minutes'
      ].max()
18.899999999999999
\end{lstlisting}
\end{frame}

% \begin{frame}[fragile]{Applying functions to cells, columns, rows}
% \begin{itemize}
%     \item Apply function to each column:
% 
% \begin{lstlisting}
% >>> def maxmethod(x):
%         return x.max()
% >>> df.apply(maxmethod)
% 
% State                        WY
% Account length              243
% Area code                   510
% International plan          Yes
% Voice mail plan             Yes
% [...]
% Churn                         1
% dtype: object
% \end{lstlisting}
% 
% \item Apply function to each row: \\
%     \texttt{df.apply(maxmethod, axis=1)}
% \end{itemize}
% \end{frame}

\begin{frame}[fragile]{Renaming things}
Renaming columns / index labels:                                                               \begin{lstlisting}
>>> df = df.rename(columns={'one': 'foo', 'two': 'bar'})
>>> df = df.rename(index={'a': 'apple', 'b': 'banana', 'd': 'durian'})
\end{lstlisting}

Replace values in the \texttt{Voice mail plan} column with proper Boolean values:
\begin{lstlisting}
>>> d = {'No' : False, 'Yes' : True}
>>> df = df.replace({'Voice mail plan': d})
\end{lstlisting}

\end{frame}


\begin{frame}[fragile]{Grouping data}
Recipe:
\begin{lstlisting}
>>> df.groupby(by=grouping_columns)[columns_to_show].function()
\end{lstlisting}

\begin{enumerate}
\item First, collect all values in columns specified
    in \texttt{grouping\_columns}.
    These will form the rows in the result.
\item Then, select columns of interest (\texttt{columns\_to\_show}).
    If not given, return all other columns.
\item Finally, apply a function to aggregate the data for each group;
    e.g. \texttt{min(), max(), mean()}.
\end{enumerate}
\end{frame}


\begin{frame}[fragile]{Grouping example}
\begin{lstlisting}
>>> columns_to_show = ['Total day minutes', 'Total eve minutes']
>>> df.groupby(['Churn'])[columns_to_show].mean()
      Total day minutes  Total eve minutes
Churn
False           175.175            199.043
True            206.914            212.410
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Contingency table}
Count clients across two variables:
\begin{lstlisting}
>>> pd.crosstab(df['Churn'], df['International plan'])
International plan  False  True
Churn
False                2664    186
True                  346    137
\end{lstlisting}
\pause
\begin{lstlisting}
>>> pd.crosstab(df['Churn'], df['Voice mail plan'], normalize=True)
Voice mail plan     False     True
Churn
False            0.602460  0.252625
True             0.120912  0.024002
\end{lstlisting}

Most clients are loyal, do not use extra plans.
\end{frame}

\begin{frame}[fragile]{Adding columns}
Add a new column based on other columns:
\begin{lstlisting}
>>> df['Total charge'] = (
        df['Total day charge']
        + df['Total eve charge']
        + df['Total night charge']
        + df['Total intl charge'])
>>> df.head()
\end{lstlisting}

Delete rows / columns:

\begin{lstlisting}
>>> # get rid of just created column
>>> df = df.drop(['Total charge'], axis=1)
>>> # and here's how you can delete rows
>>> df = df.drop([1, 2])
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Predicting churn}
\begin{block}{Goal}
    Find variables which predict a large amount of churn;\\
    i.e., show a higher churn \emph{rate} than average (not absolute numbers).
\end{block}

\begin{lstlisting}
>>> pd.crosstab(df['Churn'], df['International plan'], margins=True)
International plan    No    Yes      All
Churn
False               2664    186     2850
True                 346    137      483
All                 3010    323     3333
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Plotting I}
\begin{lstlisting}
%matplotlib inline
import seaborn as sns
sns.countplot(x='International plan', hue='Churn', data=df);
\end{lstlisting}
\begin{columns}
\column{0.7\textwidth}
\includegraphics[height=0.6\textheight]{fig/telecomplot}
\column{0.3\textwidth}

With international plan, churn rate is much higher!

\vspace{1em}
Maybe because of large expenses with international calls?
\end{columns}
\end{frame}


\begin{frame}[fragile]{Plotting II}
\begin{lstlisting}
sns.countplot(x='Customer service calls', hue='Churn', data=df);
\end{lstlisting}

\begin{columns}
\column{0.7\textwidth}
\includegraphics[height=0.6\textheight]{fig/telecomplot2}
\column{0.3\textwidth}
With 4 or more calls, churn rate sharply increases!
\end{columns}
\end{frame}

\begin{frame}[fragile]{Adding a new column}
\begin{lstlisting}
>>> df['Many_service_calls'] = (df['Customer service calls'] > 3)
>>> pd.crosstab(df['Many_service_calls'], df['Churn'], margins=True)
Churn               False  True   All
Many_service_calls
False                2721   345  3066
True                  129   138   267
All                  2850   483  3333
\end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Plotting the new column}
\begin{lstlisting}
>>> sns.countplot(x='Many_service_calls', hue='Churn', data=df);
\end{lstlisting}

\begin{columns}
\column{0.7\textwidth}
\includegraphics[height=0.4\textheight]{fig/telecomplot3}
\column{0.3\textwidth}
With 4 or more calls, churn rate sharply increases!
\end{columns}
\end{frame}

\begin{frame}[fragile]{Combining two good predictors}
\begin{lstlisting}
>>> pd.crosstab(df['Many_service_calls'] & df['International plan'], df['Churn'])
Churn  False  True
row_0
False   2841    464
True       9     19
\end{lstlisting}
\begin{itemize}
\item These two variables predict the right outcome in \\
    (2841 + 19) / 3333 = 85.8 \% of cases.
\item Slightly better than majority baseline: \\
    2850 / 3333 = 85.5 \%
    (proportion of loyal clients)
\item We can probably do better with advanced methods. \\
    \structure{But}: good to know how well a simple method performs
\end{itemize}
\end{frame}

\begin{frame}[fragile]{Checking a correlation}
\begin{lstlisting}
>>> df['Total day minutes'].corr(df['Total eve minutes'])
0.007042510993777701
>>> sns.jointplot('Total day minutes', 'Total eve minutes', data=df)
\end{lstlisting}

\includegraphics[height=0.6\textheight]{fig/scatter}
\end{frame}

\begin{frame}[fragile]{After exploratory data analysis}
With exploratory data analysis,
we looked at a handful of variables
and created a rule:

\begin{lstlisting}
if many_service_calls and international_plan:
    churn = 1
else:
    churn = 0
\end{lstlisting}

Next steps:
\begin{description}
    \item[Hypothesis testing] Confirm findings with statistical tests
    \item[Machine Learning] Automatically find more patterns:
        \begin{itemize}
            \item Automatically learn many rules
            \item Create better models from thousands of variables
        \end{itemize}
\end{description}
\end{frame}

\begin{frame}{References}
Materials used for these slides:
\begin{itemize}
\item \url{https://www.cs.umd.edu/class/fall2018/cmsc320/}
    specifically lecture 11
    % https://www.cs.umd.edu/class/fall2018/cmsc320/lecs/cmsc320_f2018_lec11.pdf
\item \url{https://mlcourse.ai/}
    specifically lecture 1
\end{itemize}

\vspace{1em}
Documentation:
\begin{itemize}
    \item \url{http://pandas.pydata.org/pandas-docs/stable/}
    \item \url{http://seaborn.pydata.org/tutorial.html}
\end{itemize}
\end{frame}

\end{document}
